# README

##Dependencies

Ruby 2.3.2, Rails 5, Postgres 9.6.1

##Instructions

1. Run `bundle install`
2. Create the database `rake db:setup`
3. Seed the database with `rake db:fixtures:load`
4. Run the server with `rails server`


##How to run the test suite

1. Setup the test database `rake db:test:prepare`
2. Run the tests `rake test`

##Notes

1. For requirement 4, level, strength, dexterity, intelligence and vitality parameters are maximums instead of minimums as shown in the example. I made it consistent with requirement 6 which returned users that are less than the queried level.

2. For requirement 6, User 2 and 3 are not able to equip Item2 and Item3 as they not high level enough. Changed all item levels to 10.
