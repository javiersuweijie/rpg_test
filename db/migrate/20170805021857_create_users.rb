class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :level, null: false
      t.integer :job, null: false
      t.string  :name
    end
    create_table :items do |t|
      t.integer :level, null: false
      t.integer :strength, null: false
      t.integer :dexterity, null: false
      t.integer :intelligence, null: false
      t.integer :vitality, null: false
      t.string  :name
    end
    add_index :items, [:level, :strength, :dexterity, :intelligence, :vitality], unique: true, name: 'item_unq_index'
  end
end
