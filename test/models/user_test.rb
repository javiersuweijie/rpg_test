require "test_helper"
class UserTest < ActiveSupport::TestCase

  test "User can be created" do
    user = User.new(level:15, job: :hunter)
    assert(user.valid?)
  end

  test "User requires job" do
    user = User.new(level: 15, name:"test")
    refute(user.valid?)
  end

  test "User requires level" do
    user = User.new(job: :mage, name:"test")
    refute(user.valid?)
  end

  test "User level between [1..20]" do
    user = User.create(level:30, job: :barbarian)
    refute(user.valid?, "level 30") 
    user.level = 20
    assert(user.valid?, "level 20")
    user.level = 0
    refute(user.valid?, "level 0")
    user.level = 1
    assert(user.valid?, "level 1")
  end 

  test "User can have items" do
    user = users(:two)
    user.items << items(:one)
  end

  test "User cannot have more than 5 items" do
    user = users(:one)
    item = items(:item_4)
    user.items << item
    assert_raise ActiveRecord::RecordInvalid do
      user.items << item
    end
  end

  test "User cannot have items with a higher level" do
    user = users(:two)
    item = items(:item_4)
    assert_raise ActiveRecord::RecordInvalid do
      user.items << item
    end
  end

  test "User total attributes add up" do
    user_1 = users(:test_1)
    user_2 = users(:test_2)
    user_3 = users(:test_3)

    assert_equal(130, user_1.user_attribute)
    assert_equal(220, user_2.user_attribute)
    assert_equal(200, user_3.user_attribute)
  end

  test "User with sorted attributes" do
    users = User.with_user_attribute(:barbarian).order_by_user_attribute
    attributes = users.map(&:_user_attribute)
    refute(attributes.empty?)
    sorted = attributes.sort.reverse
    assert_equal(sorted, attributes)
  end

  test "User with level higher than X" do
    users = User.max_level(10)
    assert(users.all? {|user| user.level <= 10})
  end

  test "User attributes with items" do
    user = users(:hunter)
    assert_equal(6, user.user_attribute)
  end
end
