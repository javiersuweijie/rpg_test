class ItemTest < ActiveSupport::TestCase

  test "Item can be created" do
    item = Item.new(level: 10, strength: 2, vitality: 2, intelligence: 10, dexterity: 3)
    assert(item.valid?)
  end

  test "Item level between [1,20]" do
    item = Item.create(level: 30, strength: 1, vitality: 1, intelligence: 1, dexterity: 1)
    refute(item.valid?, "level 30") 
    item.level = 20
    assert(item.valid?, "level 20")
    item.level = 0
    refute(item.valid?, "level 0")
    item.level = 1
    assert(item.valid?, "level 1")
  end

  test "Item stats cannot be negative" do
    item = Item.new(level: 10, strength: -2, vitality: 2, intelligence: 10, dexterity: 3)
    refute(item.valid?)
    item = Item.new(level: 10, strength: 2, vitality: -2, intelligence: 10, dexterity: 3)
    refute(item.valid?)
    item = Item.new(level: 10, strength: 2, vitality: 2, intelligence: -10, dexterity: 3)
    refute(item.valid?)
    item = Item.new(level: 10, strength: 2, vitality: 2, intelligence: 10, dexterity: -3)
    refute(item.valid?)
  end

  test "Item stats can be zero" do
    item = Item.new(level: 10, strength: 2, vitality: 2, intelligence: 10, dexterity: 0)
    assert(item.valid?)
  end

  test "Items cannot have duplicate lvl and stats" do
    assert_raise ActiveRecord::RecordNotUnique do
      Item.create(level: 1, strength: 1, dexterity: 1, intelligence: 1, vitality:1)
    end
  end

  test "Item stats cannot be more than 4 times level" do
    item = Item.new(level:1, strength: 2, vitality: 2, intelligence: 2, dexterity: 2)
    refute(item.valid?)
  end

  test "Item max level" do
    items = Item.max_level(10)
    assert(items.all? {|item| item.level <=10})
  end

  test "Item max dexterity" do
    items = Item.max_dexterity(5)
    assert(items.all? {|item| item.dexterity <= 5})
  end

  test "Item max intelligence" do
    items = Item.max_intelligence(5)
    assert(items.all? {|item| item.intelligence <= 5})
  end
  
  test "Item max vitality" do
    items = Item.max_vitality(5)
    assert(items.all? {|item| item.vitality <= 5})
  end
  
  test "Item max strength" do
    items = Item.max_strength(5)
    assert(items.all? {|item| item.strength <= 5})
  end

  test "Item ordered by job (barbarian)" do
    items = Item.order_by_job("barbarian")
    strengths = items.map {|item| item.strength}
    sorted = strengths.sort
    assert_equal(strengths, sorted)
  end

  test "Item ordered by job (mage)" do
    items = Item.order_by_job("mage")
    intelligence = items.map {|item| item.intelligence}
    sorted = intelligence.sort
    assert_equal(intelligence, sorted)
  end

  test "Item ordered by job (hunter)" do
    items = Item.order_by_job("hunter")
    dexteritys = items.map {|item| item.dexterity}
    sorted = dexteritys.sort
    assert_equal(dexteritys, sorted)
  end

end
