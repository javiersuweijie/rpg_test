class UsersControllerTest < ActionDispatch::IntegrationTest

  test "get users with level and job and limit" do
    get users_url, xhr: true, params: {level: 20, job: 'Barbarian', limit: 2}
    assert_response(:success)

    users = assigns(:users).all
    assert_equal(2, users.to_a.count)

    attributes = users.map(&:_user_attribute)
    sorted = attributes.sort.reverse

    assert_equal(sorted, attributes)

    assert(users.all? {|user| user.level <= 20})
  end

  test "get users without job" do
    get users_url, xhr: true
    assert_response(:error)
  end
end
