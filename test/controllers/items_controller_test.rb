class ItemsControllerTest < ActionDispatch::IntegrationTest

  test "get items without required fields" do
    get items_url, xhr: true
    assert_response(:error)
  end

  test "get items with required fields" do
    get items_url, params: {level: 10, job: "Barbarian"}, xhr: true
    items = assigns(:items)
    levels = items.map {|i| i.level}
    assert(levels.all? {|l| l <= 10})

    strengths = items.map {|i| i.strength}
    sorted = strengths.sort
    assert_equal(strengths, sorted)
    assert_response(:success)
  end

  test "get items with max strength" do
    get items_url, params: {level: 10, job: "Barbarian", strength: 5}, xhr: true
    items = assigns(:items)
    strengths = items.map {|item| item.strength}
    assert(strengths.all? {|strength| strength <= 5})
  end

  test "get items with limit" do
    get items_url, params: {level: 10, job: "Barbarian", limit: 2}, xhr: true
    items = assigns(:items)
    assert_equal(items.count, 2)
  end

end
