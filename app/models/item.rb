class Item < ApplicationRecord
  has_many :user_items, class_name: "UserItem"

  validates :level, presence: true, inclusion: {in: (1..20)}
  validates :strength, presence: true, numericality: {greater_than_or_equal_to: 0}
  validates :dexterity, presence: true, numericality: {greater_than_or_equal_to: 0}
  validates :intelligence, presence: true, numericality: {greater_than_or_equal_to: 0}
  validates :vitality, presence: true, numericality: {greater_than_or_equal_to: 0}
  validate :item_stats_less_than_level
  validates_associated :user_items

  scope :max_level, -> (level) {where("items.level <= ?", level)}
  scope :max_strength, -> (strength) { where("items.strength <= ?", strength)}
  scope :max_intelligence, -> (intelligence) { where("items.intelligence <= ?", intelligence)}
  scope :max_dexterity, -> (dexterity) { where("items.dexterity <= ?", dexterity)}
  scope :max_vitality, -> (vitality) { where("items.vitality <= ?", vitality)}

  scope :order_by_job, -> (job) {
    if job == "barbarian"
      order(strength: :asc, vitality: :asc)
    elsif job == "mage"
      order(intelligence: :asc, vitality: :asc)
    elsif job == "hunter"
      order(dexterity: :asc, vitality: :asc)
    end
  }

  def item_stats_less_than_level
    total = strength + dexterity + vitality + intelligence
    if total > level * 4
      errors.add(:level, "is too low for the stats")
    end
  end
end
