class UserItem < ApplicationRecord
  belongs_to :item
  belongs_to :user

  validate :not_more_than_five_items
  validate :item_level_not_more_than_user_level

  def not_more_than_five_items
    if user.items.count >= 5
      errors.add(:user, "cannot have more than 5 items")
    end
  end

  def item_level_not_more_than_user_level
    if item.level > user.level
      errors.add(:user, "level too low")
    end
  end
end
