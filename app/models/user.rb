class User < ApplicationRecord
  enum job: ['mage', 'barbarian', 'hunter']
  validates :level, presence: true, inclusion: {in: (1..20)}
  validates :job, presence: true
  validates_associated :user_items

  has_many :user_items, class_name: "UserItem"
  has_many :items, through: :user_items

  scope :order_by_user_attribute, -> () {
    order("_user_attribute desc")
  }

  scope :max_level, -> (level) {where("users.level <= ?", level)}
  
  def self.with_user_attribute(job)

    query = "users.id as id, "\
            "users.job as job,"\
            "users.level as level,"\
            "(users.level * 3 + COALESCE(SUM(items.#{User.primary_stat(job)}),0)) + "\
            "(users.level * 3 + COALESCE(SUM(items.vitality),0)) AS _user_attribute"

    User.left_outer_joins(:items)
        .group("users.id","users.level")
        .where(job: job)
        .select(query)
  end

  # experimental - not used
  def self.with_user_attribute_any
    query = "users.id as id, "\
            "users.job as job,"\
            "users.level as level,"\
            "CASE "\
              "WHEN job = 0 THEN (users.level * 3 + COALESCE(SUM(items.intelligence),0))"\
              "WHEN job = 1 THEN (users.level * 3 + COALESCE(SUM(items.strength),0))"\
              "WHEN job = 2 THEN (users.level * 3 + COALESCE(SUM(items.dexterity),0))"\
            "END + (users.level * 3 + COALESCE(SUM(items.vitality),0)) AS _user_attribute"

    User.left_outer_joins(:items)
        .group("users.id","users.level")
        .select(query)
  end

  def user_attribute
    User.with_user_attribute(job).where(id: id)[0]._user_attribute
  end

  private
  def self.primary_stat(job)
    job = job.to_s
    if job == 'barbarian'
      return "strength"
    elsif job == 'mage'
      return "intelligence"
    elsif job == 'hunter'
      return "dexterity"
    end
  end

end
