class UsersController < ApplicationController
  def index
    if params[:job].nil?
      render json: {}, status: :error
    else
      @users = User.with_user_attribute(params[:job].downcase).order_by_user_attribute
      @users = @users.limit(params[:limit]) if params[:limit]
      @users = @users.max_level(params[:level]) if params[:level]
      render json: @users.to_json
    end
  end
end
