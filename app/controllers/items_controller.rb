class ItemsController < ApplicationController
  def index
    if params[:level].nil? or params[:job].nil?
      render json: {}, status: :error
    else
      @items = Item.max_level(params[:level]).order_by_job(params[:job].downcase)
      @items = @items.max_strength(params[:strength]) if params[:strength]
      @items = @items.max_vitality(params[:vitality]) if params[:vitality]
      @items = @items.max_intelligence(params[:intelligence]) if params[:intelligence]
      @items = @items.max_dexterity(params[:dexterity]) if params[:dexterity]
      @items = @items.limit(params[:limit]) if params[:limit]
      render json: @items.to_json
    end
  end

  #private

  #def item_params
    #params.require(:items).permit(:level, :job)
  #end
end
